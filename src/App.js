import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
class App extends Component {
  details(index){
    let todos=this.state.todos;
    let todo=todos.find(function(todo){
      return todo.counter===index
    });
    console.log(todo);
  }

  removeTodo(index){
    let todos=this.state.todos;
    let todo=todos.find(function(todo){
      return todo.counter===index
    });
    todos.splice(todo,1);
    this.setState({
      todos:todos
    });

  }

addTodo(event){
  event.preventDefault();
  let name=this.refs.name.value;
  let completed=this.refs.completed.value;
  let counter =this.state.counter;
  let todo={
    name,
    completed,
    counter
  };
  counter++;
  let todos =this.state.todos;
  
  todos.push(todo);

  this.setState({
    todos:todos,
    counter:counter
  });

this.refs.todoForm.reset();

}

constructor(){
  super();
  this.addTodo=this.addTodo.bind(this);
  this.removeTodo=this.removeTodo.bind(this);
  this.details=this.details.bind(this);
  this.state={
    todos:[],
    title:'Rect application for CRUD',
    counter:0
  }
}

  render() {
    let title=this.state.title;
    let todos=this.state.todos;
    return (
      <div className="container">
        <h1>React</h1>
        <form className="form" ref="todoForm">
          <input className="form-control" type="text" ref="name" placeholder="what to do" />
          <input className="form-control" type="text" ref="completed" />
          <button onClick={this.addTodo} className="btn btn-success">Add To Do</button>
        </form> 
        <ul className="list-group">
          {todos.map((todo => <li  className="list-group-item active" key={todo.counter}>{todo.name}
            <button className="btn btn-danger" onClick={this.removeTodo.bind(null ,todo.counter)}>Remove Todo</button>
            <button className="btn btn-default" onClick={this.details.bind(null,todo.counter)}>Details</button>
            </li>))}
        </ul>
      </div>
    );
  }
}

export default App;
